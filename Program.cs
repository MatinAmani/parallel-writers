﻿using System;
using System.IO;
using System.Threading;

namespace Readers_and_Writers
{
	class Program
	{
		static void Main(string[] args)
		{
			Writer writer0 = new Writer("W0", "G:\\data.txt");
			Writer writer1 = new Writer("W1", "G:\\data.txt");
			Writer writer2 = new Writer("W2", "G:\\data.txt");
			Writer writer3 = new Writer("W3", "G:\\data.txt");
			Writer writer4 = new Writer("W4", "G:\\data.txt");

			Thread[] threads = new Thread[5];
			threads[0] = new Thread(new ThreadStart(writer0.Go));
			threads[1] = new Thread(new ThreadStart(writer1.Go));
			threads[2] = new Thread(new ThreadStart(writer2.Go));
			threads[3] = new Thread(new ThreadStart(writer3.Go));
			threads[4] = new Thread(new ThreadStart(writer4.Go));

			threads[0].Start();
			threads[1].Start();
			threads[2].Start();
			threads[3].Start();
			threads[4].Start();

			//Try_Thread(threads[0]);
			//Try_Thread(threads[1]);
			//Try_Thread(threads[2]);
			//Try_Thread(threads[3]);
			//Try_Thread(threads[4]);
		}

		static void Try_Thread(Thread thread)
		{
			for (int i = 0; i < 100; i++)
			{
				try
				{
					thread.Start();
					break;
				}
				catch (Exception e)
				{
					Thread.Sleep(300);
					//Console.WriteLine(e.Message);
				}
			}
			
			//thread.Start();
		}
	}

	public class Writer
	{
		private string FilePath;
		private string Name;
		private Random Rand = new Random();

		public Writer(string name, string path)
		{
			this.Name = name;
			this.FilePath = path;
		}

		public void Write()
		{
			/*
			string data = this.RandomData();
			Console.WriteLine($"{this.Name} is Writing...");
			File.AppendAllText(this.FilePath, $"Writer {this.Name}: " + data + "\r\n");
			*/

			string data = this.RandomData();
			Console.WriteLine($"Writer {this.Name}: " + data + "\r\n");
		}

		public void Go()
		{
			for (int i = 0; i < 50; i++)
			{
				this.Write();
				Thread.Sleep(1000);
			}
		}

		private string RandomData()
		{
			string data = "";
			byte[] array = new byte[10];
			this.Rand.NextBytes(array);
			foreach (byte b in array)
			{
				data += (char)b;
			}
			return data;
		}
	}

	public class Reader
	{
		private FileInfo file;
		private string Name;
		public Reader(string name, string path)
		{
			this.Name = name;
			this.file = new FileInfo(path);
		}
	}
}
